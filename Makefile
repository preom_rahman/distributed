CC      = mpicc
CFLAGS  = -Wall -std=c99 -fnested-functions
OBJECTS = main.o 
LIBPATH = -L /opt/local/lib
multtable: $(OBJECTS) 
	$(CC)  -o $@  $(OBJECTS) $(LIBPATH) -lgmp

clean:
	rm *.o multtable

